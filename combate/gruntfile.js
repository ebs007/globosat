module.exports = function (grunt) {
    // config
    grunt.initConfig({
        // pkg
        pkg: grunt.file.readJSON('./package.json'),
        // make a zipfile
        compress: {
            main: {
                options: {
                    mode: 'zip'
                },
                expand: true,
                cwd: 'assets/',
                src: ['**/*'],
                dest: 'public/'
            }
        },
        // minify html
        minifyHtml: {
            options: {
                cdata: true
            },
            dist: {
                files: {
                    'public/index.html': 'src/index.html'
                }
            }
        },
        // images min
        imagemin: {
            options: {
                progressive: true
            },
            files: {
                expand: true,
                cwd: 'assets/img/',
                src: ['*.{png,jpg,gif}'],
                dest: 'public/img'
            }
        },
        // test js
        jasmine: {
            src: 'assets/js/hello.js',
            options: {
                specs: 'spec/hello.Spec.js'
            }
        },
        // validate js
        jshint: {
            options: {
                reporter: require('jshint-stylish'),
                'curly': true,
                'newcap': true,
                'eqeqeq': true,
                'undef': true,
                'devel': true,
                'debug': true,
                'globals': {
                    '$': true,
                    'jQuery': true,
                    'angular': true,
                    'console': true,
                    'document': true,
                    'navigator': true,
                    'window': true
                }
            },
            ignores: [
                "/components/jQuery/dist/jquery.js",
                "/components/bootstrap-sass/assets/javascripts/bootstrap.js",
                "/components/angular/angular.js"
            ],
            all: "assets/js/*.js",
            prod: {
                options: {
                    'devel': false,
                    'debug': false
                },
                files: {
                    src: "assets/js/*.js"
                }
            }
        },
        // sass
        sass: {
            options: {
                sourceMap: true,
                outputStyle: 'compressed'
            },
            dist: {
                files: {
                    "public/css/style.min.css": ["assets/scss/main.scss", 'assets/css/*.css']
                }
            }
        },
        // mininfy js
        uglify: {
            options: {
                mangle: false
            },
            my_files: {
                options: {
                    sourceMap: true,
                    sourceMapName: "public/js/main.min.map",
                    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> */'
                },
                files: {
                    "public/js/main.min.js": [
                        "bower_components/jQuery/dist/jquery.js",
                        "bower_components/bootstrap-sass/assets/javascripts/bootstrap.js",
                        "bower_components/angular/angular.js",
                        "assets/js/*.js"
                    ]
                }
            }
        },
        // watch
        watch: {
            options: {
                livereload: true
            },
            html: {
                files: ['src/index.html'],
                tasks: ['html']
            },
            sass: {
                files: ["assets/scss/*.scss"],
                tasks: ["css"]
            },
            js: {
                files: ["assets/js/*.js"],
                tasks: ["js"]
            },
            img: {
                files: ["assets/img/*.*"],
                tasks: ["img"]
            }
            /*js_test:{
                files: 'assets/js/hello.js',
                tasks: 'test'
            }*/
            // css:{
            //     files: 'assets/temp/main.css',
            //     tasks: 'cssmin'
            // }
        },
        // live server with reload
        connect: {
            server: {
                options: {
                    port: 9000,
                    base: 'public/',
                    hostname: 'localhost',
                    livereload: true
                }
            }
        }
    });
    // load plugins
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-minify-html');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // tasks
    grunt.registerTask('css', ['sass:dist']);
    grunt.registerTask('js', ['jshint:prod', 'uglify:my_files']);
    grunt.registerTask('live', ['connect', 'watch']);
    grunt.registerTask('conn', ['connect:server']);
    grunt.registerTask('mm-js', ['uglify:my_files']);
    grunt.registerTask('img', ['imagemin']);
    grunt.registerTask('sprite', ['sprite:all']);
    grunt.registerTask('test', ['jasmine']);
    grunt.registerTask('zip', ['compress']);
    grunt.registerTask('html', ['minifyHtml']);
    grunt.registerTask('build', ['css', 'js', 'img', 'html']);

};