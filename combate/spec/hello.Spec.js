describe('Olá', function () {
    var hello;

    beforeEach(function () {
        hello = new Hello();
    });

    it('Diga meu nome corretamente', function () {
        expect(hello.sayHi('Erick')).toEqual('Meu nome é Erick e estou aprendendo Grunt com Jasmine!');
    });
});
